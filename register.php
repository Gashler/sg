<p>
    A link to the downloads will be sent to your email address. Your subscription makes it possible for me to give away free stuff and keep you posted on my latest articles and giveaways. You can unsubscribe at any time.
</p>
<div>
    <label>
        <input type="checkbox" id="confirm">
        OK, cool!
    </label>
</div>
<?php echo do_shortcode('[theme-my-login]') ?>
<script>

	// add human check popup
	jQuery("#registerform").append('' +
		'<div id="human-check" class="pop">' +
			'<h2 style="margin-top:0;">Human Check</h2>' +
			'<p>What is the third word in this sentence?</p>' +
			'<input style="display:inline-block;" type="text" id="answer" name="answer">' +
			'<button>Submit</button>' +
		'</div>' +
	'');

jQuery("#registerform").submit(function (event) {
	if (jQuery('#answer').val() != 'the') {
		if (jQuery('#answer').val() != 'The') {
		    event.preventDefault();
		    validate();
		}
	}
});

	var alreadyChecked = false;
	function validate() {
		jQuery('input#user_login').attr('value', jQuery('input#user_email').val());
		if (!(jQuery("#confirm").is(":checked"))) return alert("You must agree to the terms.");
		else {
			if (alreadyChecked == true) {
				if (popOpen == true) {
					closePopup();
					alert("Domo arigato, Mister Roboto!");
				}
				else pop("human-check");
			}
			else {
				pop("human-check");
				alreadyChecked = true;
			}
		}
	}

	jQuery(document).ready(function() {
	    // jQuery('form').attr('onsubmit', 'return validate()');
		jQuery('input[name="user_login"]').attr('type', 'hidden');
		jQuery('#user_email').attr('placeholder', 'Your Email Address');
		

	});
</script>