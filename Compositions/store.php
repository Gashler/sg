<!-- <script src="/wp-content/plugins/media-element/build/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="/wp-content/plugins/media-element/build/mediaelementplayer.css" /> -->
<div style="position:relative">
    <h3 style="font-weight:100;">
        34 Tracks of 100% Royalty Free, High Quality Music for Only
        <div class="strike">
            $199
            <div class="line"></div>
        </div>
        $99
    </h3>
    <p><span class="highlight">Download 100% royalty free orchestrations and instrumental tracks</span> by composer Stephen Gashler. Covering many genres and moods from feel-good rock, to laid-back jazz, to powerful and inspiring, to dark and sombre, you'll find find a shmorgasbord of musical experience at an unbeatable price. Following your purchase, you'll gain isntant access to over a gigabyte of high quality recordings.</p>

    <img alt="100% Satisfaction Guarantee" src="/wp-content/themes/stephengashler/images/satisfaction-guarantee.png" width="132" height="133">
    <img alt="50 Percent Off" src="/wp-content/themes/stephengashler/images/50-percent-off.png" width="132" height="133">
</div>

<div style="display:none;" id="mobile-buy-now">
    <br>
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="ZG23D99Z2L2HU">
        <input style="border:none;" type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
    </form>
</div>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css"> -->

<?php
    function listDirectory($directory) {
        if ($handle = opendir('Compositions/mp3/' . $directory)) {
            echo '<h2>' . $directory . '</h2>';
            echo '<table>';
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $entries[] = $entry;
                }
            }
            natsort($entries);
            foreach ($entries as $entry) {
                $title = explode('.mp3', $entry);
                $title = $title[0];
                echo '<tr><td><strong>' . $title . '</strong><br><audio controls><source src="/Compositions/mp3/' . $directory . '/' . $entry . '"></audio></td></tr>';
            }
            echo '</table>';
            closedir($handle);
        }
    }
    listDirectory('Classical');
    listDirectory('Classical-Rock');
    listDirectory('Disco');
    // listDirectory('Funk');
    listDirectory('Jazz');
    listDirectory('March');
    listDirectory('Rock');
    listDirectory('World');
?>
<!-- <script>
var audio = document.createElement("audio");
audio.src = "sound.mp3";

audio.addEventListener("canplaythrough", function () {
        setTimeout(function(){
            audio.pause();
            alert("Audio Stop Successfully");
        },
        10000);
}, false);
// using jQuery
jQuery('video,audio').mediaelementplayer(/* Options */);
</script> -->
<br>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
    <input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="ZG23D99Z2L2HU">
    <input style="border:none;" type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
<script>
    if (jQuery(window).width() < 1000) {
        jQuery('#mobile-buy-now').show();
    }
</script>
