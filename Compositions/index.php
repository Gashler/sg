<p>Download professional orchestrations and instrumental tracks by composer Stephen Gashler for your movies, video games, plays, or other projects. All tracks are 100% free to use (including royalty free) for any purpose whatsoever. All that's asked is that you give credit where credit is due (and tell your friends about this awesome free resource). If you would like Stephen Gashler to score your next project, <a href="/contact">contact him for a free quote</a>.</p>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css">

<?php
	function listDirectory($directory) {
		if ($handle = opendir('Compositions/mp3/' . $directory)) {
			echo '<h2>' . $directory . '</h2>';
			echo '<table>';
		    while (false !== ($entry = readdir($handle))) {
		        $title = explode('.mp3', $entry);
				$title = $title[0];
		    	$wav = '';
		    	if ($entry != '.' && $entry != '..' && file_exists('Compositions/wav/' . $directory . '/' . $title . '.wav')) {
		    		$wav = ' <a download href="/Compositions/wav/' . $directory . '/' . $title . '.wav"><i class="fa fa-arrow-circle-down" title="Download high fedility wav file"></i></a>';
				}
		        if ($entry != "." && $entry != "..") {
		            echo '<tr><td><a href="/Compositions/mp3/' . $directory . '/' . $entry . '">' . $title . '</a></td><td><a download href="/Compositions/mp3/' . $directory . '/' . $entry . '"><i class="fa fa-arrow-circle-o-down" title="Download mp3"></i></a>' . $wav . '</td></tr>';
		        }
		    }
			echo '</table>';
		    closedir($handle);
		}
	}
	listDirectory('Classical');
	listDirectory('Classical-Rock');
	listDirectory('Dance');
	listDirectory('Disco');
	listDirectory('Funk');
	listDirectory('Jazz');
	listDirectory('Rock');
	listDirectory('World');
?>