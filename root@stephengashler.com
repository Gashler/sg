<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sg');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'asdf');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h?Fw]f]_a+>+V#<[x&M0%Xgjau|]zM{>K@Ykj/uBxiLf W?Q(-fC`e{cCGt[%j:q');
define('SECURE_AUTH_KEY',  'B-y11{V&1W42jK$ej}7a~wu3;b#In{nveXlW2^c|6;aiw*$1P+25y1JkC^X`T;[u');
define('LOGGED_IN_KEY',    'ru:zmnr(Tp6 #!0$H Wd%GgOuX.8vm.S|Axj|HX T;J_)yGQYmQz5-29(WAaM)3s');
define('NONCE_KEY',        '|w9C})}9|rNmt29[PrL}LT0IeA-fzB+^}T9T`-&JO4BPU*/+c*rJaC!~:067,C|`');
define('AUTH_SALT',        '+ OxsKPYa[F#PU>g]pRQBrj+D,RQ,@.R@3h#|mrHZ{{8AKb5i7>OIn_-fr#L+w}4');
define('SECURE_AUTH_SALT', '&L6/ab:T%OiNbkq+jAz3VTK {~NLFG+_1XzwtVrg{5Dckf7@2ZuZq~/0JN!t(ll3');
define('LOGGED_IN_SALT',   'ijYmxK61DQ9,Dt#ESkgZ7]vIcJY Hoq.I25zJ68Pb-&wSq=2^s$;vSFqk!Pv7]|~');
define('NONCE_SALT',       'g]]{hs;DHFw9:713ELK3IT8$:Pb<]QB~TKeJ|saa9ofnDd:,a$kc3Wu|ec`6y>R-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
