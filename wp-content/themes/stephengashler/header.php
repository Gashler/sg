<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <!-- <script src="jquery.js"></script> -->
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
</head>

<body <?php body_class(); ?>>
<?php if (!$_GET['store']) : ?>
    <nav class="top-nav">
        <?php wp_list_pages('title_li=&child_of=3491'); ?>
    </nav>
<?php endif ?>
    <div style="clear:both;"></div>
    <div id="page" class="hfeed site">
        <header id="masthead" class="site-header" role="banner">
            <?php
                if (isset($_GET['role'])) $role = $_GET['role'];
                else $role = get_field("role");
            ?>
            <a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                <?php if ($role == null) { ?>
                    <img src="<?php echo get_bloginfo('template_url') ?>/images/headers/stephen-gashler-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
                <?php } else { ?>
                    <img src="<?php echo get_bloginfo('template_url') ?>/images/headers/stephen-gashler-banner-<?php echo $role ?>.jpg" alt="<?php bloginfo( 'name' ); ?>" />
                <?php } ?>
            </a>
        </header><!-- #masthead -->
        <?php
            if (!$_GET['landing'] && !$_GET['store']) {
        ?>
            <div id="navbar" class="navbar">
                <nav id="site-navigation" class="navigation main-navigation" role="navigation">
                    <nav class="nav-menu">
                        <li><a href="/"><img src="<?php echo get_bloginfo('template_url') ?>/images/home.png" alt="home"  /></a></li>
                        <li><a href="/author-dude">Author</a></li>
                        <li><a href="/musician">Musician</a></li>
                        <li><a href="/performer">Performer</a></li>
                        <li><a href="/utah-artist">Artist</a></li>
                        <?php get_search_form(); ?>
                    </nav>
                </nav><!-- #site-navigation -->
            </div><!-- #navbar -->
            <?php if (!(is_front_page())) { ?>
                <div class="breadcrumbs">
                    <?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </div>
            <?php } ?>
        <?php } ?>
        <div id="main" class="site-main">
