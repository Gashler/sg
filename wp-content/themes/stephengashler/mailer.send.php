<?php
	$con=mysqli_connect("localhost","root","Penguin4Shpenguin!?","mailers");
	// Check connection
	if (mysqli_connect_errno())
	  {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  }

	$result = mysqli_query($con,"SELECT * FROM " . $_POST['table']);
	while($row = mysqli_fetch_array($result)) {
		$from = "Stephen Gashler";
		$subject = "Discounted performances at " . $row['organization'];
	    $message = "Dear " . $row['name'] . ",
			
			I'm an award-winning storyteller, puppeteer, musician, and author, and as part of my 2014 tour, I'll be performing at schools and libraries near " . $row['%organization%'] . ". Because I'll already be in your area, I'm offering discounted assemblies or performances for only $300 (normally $400) and only $150 for a second performance during the same morning, afternoon, or evening. I also offer workshops at similar rates.
			
			I've been entertaining family and young audiences for over seven years, with a large repertoire from world folktales, to US History, to anti-bullying, to creative development, and much more. To learn more, visit <a href='/tour'>/tour</a>. If you're interested, please reply soon, as bookings fill up fast.
			
			Sincerely,
			Stephen Gashler
			<a href='http://stephengashler.com'>http://stephengashler.com</a>
	    ";
	    // message lines should not exceed 70 characters (PHP rule), so wrap it
	    $message = wordwrap($message, 70);
	    // send mail
	    mail($row['email'],$subject,$message,"From: $from\n");
	    echo "Sent to " . $row['name'];
    }
?>