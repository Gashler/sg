<?php echo $_POST['table'] ?>
<p>
	Sending Mail ...
</p>
<?php
	require_once('class.phpmailer.php');

error_reporting(E_STRICT | E_ALL);

date_default_timezone_set('Etc/UTC');

require 'PHPMailerAutoload.php';



//connect to the database and select the recipients from your mailing list that have not yet been sent to
//You'll need to alter this to match your database
$mysql = mysql_connect('localhost', 'root', 'Penguin4Shpenguin!?');
mysql_select_db('mailers', $mysql);
$result = mysql_query("SELECT name, email, organization FROM " . $_POST['table'] . " WHERE sent = false", $mysql);

while ($row = mysql_fetch_array($result)) {
	echo $row['name'];
	$body = "
<p>Dear " . $row['name'] . ",</p>
<p>
Looking for fun and educational assemblies or performances? I'm an award-winning storyteller, puppeteer, musician, and author, and as part of my 2014 tour, I'll be performing at schools and libraries near " . $row['organization'] . ". Because I'll already be in your area, I'm offering discounted assemblies or performances for only $300 (normally $400) and only $150 for second performances. I also offer workshops at similar rates.
</p>
<p>
I've been entertaining family and young audiences for over seven years, with a large repertoire including folktales, US History, anti-bullying, creative development, and much more. To learn more, visit <a href='/tour'>/tour</a>. If you're interested, please reply soon, as bookings fill up fast.
</p>
<p>
Sincerely,<br>
Stephen Gashler<br>
<a href='http://stephengashler.com'>http://stephengashler.com</a>
</p>
	";
	$mail = new PHPMailer();
	$mail->From      = 'steve@stephengashler.com';
	$mail->FromName  = 'Stephen Gashler';
	$mail->Subject   = 'Discounted performances at ' . $row['organization'];
	$mail->Body      = 'Test Message';
	$file_to_attach = 'handout-small.jpg';
	$mail->AddAttachment( $file_to_attach , 'handout-small.jpg' );
    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
    $mail->msgHTML($body);
    $mail->addAddress($row['email'], $row['name']);
    //$mail->addStringAttachment($row['photo'], 'handout-small.jpg'); //Assumes the image data is stored in the DB

    if (!$mail->send()) {
        echo "Mailer Error (" . str_replace("@", "&#64;", $row["email"]) . ') ' . $mail->ErrorInfo . '<br />';
        break; //Abandon sending
    } else {
        echo "Message sent to :" . $row['name'] . ' (' . str_replace("@", "&#64;", $row['email']) . ')<br />';
        //Mark it as sent in the DB
        mysql_query(
            "UPDATE mailinglist SET sent = true WHERE email = '" . mysql_real_escape_string($row['email'], $mysql) . "'"
        );
    }
    // Clear all addresses and attachments for next loop
    $mail->clearAddresses();
    $mail->clearAttachments();
}

?>