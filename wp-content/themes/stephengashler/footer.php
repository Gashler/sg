<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
			<?php get_sidebar( 'main' ); ?>
		</div><!-- #main -->
		<footer id="footer">

			<div class="site-info">
				Copyright &copy; <?php echo date(Y) ?>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
	<script>
		var landing = "<?php echo $_GET['landing'] ?>";
	</script>
	<script src="<?php echo get_bloginfo('template_url') ?>/js/functions.js"></script>
</body>
</html>