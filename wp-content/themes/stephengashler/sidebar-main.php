<?php
/**
 * The sidebar containing the footer widget area.
 *
 * If no active widgets in this sidebar, it will be hidden completely.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<div id="secondary" class="sidebar-container" role="complementary">
    <div class="widget-area">
        <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
            <?php
                if (!$_GET['store']) {
                    dynamic_sidebar( 'sidebar-1' );
                }
                // if normal pages
                if (!$_GET['landing'] && !$_GET['store']) {
            ?>
                <?php if (!(is_page(3435)) && !(is_page(3487)) && !(is_page(4116))) { ?>
                    <a href="/performer/storyteller/">
                        <div class="block">
                            <img src="/wp-content/uploads/2013/08/IMG_5803-223x300.jpg" alt="Stephen Gashler - Storyteller" /><br />
                            <p>
                                <strong>Stephen Gashler</strong><br />
                                Award-winning Storyteller &amp; Comedian
                            </p>
                            <button>Learn More</button>
                        </div>
                    </a>
                <?php } ?>
                <?php if (!(is_page(4016)) && !(is_page(3478)) && !(is_single(3959))) { ?>
                    <a href="/author/novelist/prisoner-molepeople/">
                        <div class="block">
                            <img src="/wp-content/uploads/2013/08/POTMP-cover-blue-198x300.jpg" alt="Prisoner of the Molepeople" /><br />
                            <p>
                                <strong>Prisoner of the Molepeople</strong><br />
                                Stephen Gashler's hilarious new novel and audio play
                            </p>
                            <button>Learn More</button>
                        </div>
                    </a>
                <?php } ?>
                <?php if (!(is_page(3693)) && !(is_page(3478))) { ?>
                    <a href="/author/novelist/the-bent-sword/">
                        <div class="block">
                            <img src="/wp-content/uploads/2011/02/The-Bent-Sword.jpg" alt="The Bent Sword" />
                            <p>
                                <strong>The Bent Sword</strong><br />
                                Stephen Gashler's hilarious Whitney Award nominated Young Adult Fantasy novel and full length musical
                            </p>
                            <button>Learn More</button>
                        </div>
                    </a>
                <?php } ?>
            <?php
        } elseif ($_GET['store'] && (is_page(6844) || is_page(6835))) {
            ?>
            <div style="text-align:center">
                <!-- <h3>
                    2+ Hours of Stock Music
                    <div style="font-weight:100;">
                        45 High Quality, 100% Royalty Free Tracks
                        <br>
                        <div class="strike">
                            $199
                            <div class="line"></div>
                        </div>
                        $99
                    </div>
                </h3> -->
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="ZG23D99Z2L2HU">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
            </div>
            <?php
                } elseif ($_GET['landing']) {
                // if landing pages
            ?>
                <h2 style="margin-top:0;">Request a Quote</h2>
                <?php
                    if (isset($_REQUEST['email']))
                    //if "email" is filled out, send email
                      {
                      //send email
                      $email = $_REQUEST['email'];
                      $subject = $_REQUEST['subject'];
                      $message = "
Name:" . $_POST['full_name'] . "

Company:" . $_POST['company'] . "

Email:" . $_POST['email'] . "

Phone:" . $_POST['phone'] . "

Type of Project:" . $_POST['subject'] . "

Deadline:" . $_POST['deadline'] . "

Description:" . $_POST['description'] . "
";

                      mail("americanknight@gmail.com", "Quote Request:" . $subject,
                      $message, "From:" . $email);
                ?>
                    <p style="background:rgb(255,255,192); border-radius:2px; box-shadow:1px 1px 3px gray; padding:.5em;">Thank you for your request. I'll try to get back to you as soon as possible.</p>
                <?php
                      }
                    else
                    //if "email" is not filled out, display the form
                      { ?>
                          <?php
                              $role = $_GET['role'];
                            if ($role == "composer") {
                          ?>
                                <p>To begin, please provide information about your project.</p>
                          <?php } else { ?>
                                <p>Regular rate is $35. Please provide information about your project for an estimation of hours.</p>
                        <?php } ?>
                          <form method='post' action="<?php the_permalink() ?>">
                                <label>Name:</label>
                              <input name='full_name' type='text'>
                                <label>Company <span style="font-weight:normal;">(optional)</span>:</label>
                              <input name='company' type='text'>
                                <label>Email:</label>
                              <input name='email' type='text'>
                              <label>Phone:</label>
                              <input name='phone' type="text">
                              <label>Type of Project:</label>
                              <input name='subject' type='text'>
                              <label>Deadline:</label>
                              <input name='deadline' type='text'>
                              <label>Project Details:</label>
                              <textarea name='description' rows="10"></textarea>
                              <button style="font-size:12pt; padding:10px 20px; display:block; width:100%; margin-top:1em;">Request Quote</button>
                          </form>
                <?php } ?>
            <?php } ?>
        <?php endif; ?>
    </div><!-- .widget-area -->
</div><!-- #secondary -->
