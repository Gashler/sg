<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<header class="page-header">
				<?php
					$_GET['landing'] = $_GET['landing'];
					if (!$_GET['landing']) {
				?>
				<h1 class="page-title"><?php _e( 'Not found', 'twentythirteen' ); ?></h1>
				<?php } else { ?>
					<h1>Request Sent</h1>
				<?php } ?>
			</header>

			<div class="page-wrapper">
				<div class="page-content">
					<?php
						if (!$_GET['landing']) {
					?>
						<h2><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it?', 'twentythirteen' ); ?></h2>
						<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentythirteen' ); ?></p>
						<?php get_search_form(); ?>
					<?php } ?>
				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>