<div id="subscriberGiveaways">
	<div id="giveaways">
		<a href="/roles/author/novelist/prisoner-molepeople/">
			<img src="<?php echo get_bloginfo('template_url') ?>/images/weekly-giveaways.png" alt="Weekly Giveways" id="weekly-giveaways">
		</a>
		<img style="float:left; margin-right:1em; scale="0" class="size-medium wp-image-4013" alt="Prisoner of the Molepeople" src="/wp-content/uploads/2013/08/POTMP-cover-blue.jpg" height="150" width="99">
		<strong style="font-size:1.85em; font-family:oswald; margin-top:3px; display:block; line-height:1.5em;">This Week's Winner</strong>
		<table>
			<tr>
				<th>Name:</th>
				<td>Erika Stach</td>
			</tr>
			<tr>
				<th>Giveaway:</th>
				<td><a href="/roles/author/novelist/prisoner-molepeople/">Prisoner of the Molepeople (eBook)</a></td>
			</tr>
		</table>
	</div>
	<form action="/?wpr-optin=1" method="post" id="subscribe">
		<p><strong>Enter to Win Weekly Give-aways</strong> and to subscribe to Steve's blog.</p>
		  	<input name="blogsubscription" value="all" type="hidden">
		    <input name="newsletter" value="1" type="hidden">
		    <input name="fid" value="1" type="hidden">
		    <div style="float:left; max-width:150px;">
				<input name="name" type="text" placeholder="Full Name">
				<input name="email" type="text" placeholder="Email Address">
			</div>
			<div style="float:left;">
				<input value="Subscribe" class="button-primary" type="submit" style="padding:10px 30px;">				
			</div>
	</form>
</div><!-- subscriberGiveways -->
