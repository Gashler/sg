<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		
		<!-- AddThis Button BEGIN -->
		<div class="addthis_container" style="position:absolute; z-index:501;">
			<div class="addthis_toolbox addthis_floating_style addthis_counter_style">
			<a class="addthis_button_facebook_like" fb:like:layout="box_count"></a>
			<a class="addthis_button_tweet" tw:count="vertical"></a>
			<a class="addthis_button_google_plusone" g:plusone:size="tall"></a>
			<a class="addthis_counter"></a>
			</div>
			<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f9024373ebb49dc"></script>
		</div>
		<!-- AddThis Button END -->
		
		<?php
			if (isset($_GET['landing'])) $_GET['landing'] = $_GET['landing'];
			else {
				$_GET['landing'] = get_field("landing");
				$_GET['landing'] = $_GET['landing'][0];
			}
			if (!$_GET['landing']) { ?>
			<?php //include "subscribe.php" ?>
		<?php } ?>
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php twentythirteen_post_nav(); ?>
				<?php comments_template(); ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>