<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <?php
            if (!(is_page(4150)) && !$_GET['landing']) {
            // include "subscribe.php";
        }
        ?>
        <div id="content" class="site-content" role="main">

            <?php /* The loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">
                        <?php
                            if ($_GET['landing']) {
                        ?>
                            <h1 class="entry-title"><?php the_title(); ?> For Hire</h1>
                        <?php } else { ?>
                            <h1 class="entry-title"><?php the_title(); ?></h1>
                        <?php } ?>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <?php the_content(); ?>
                        <?php if (!(is_page(129)) && !(is_page(123))) { ?>
                            <?php echo do_shortcode("[sb_child_list]"); ?>
                        <?php } ?>
                        <div style="clear:both;"></div>
                    </div><!-- .entry-content -->

                    <footer class="entry-meta">
                    </footer><!-- .entry-meta -->
                </article><!-- #post -->
            <?php endwhile; ?>

        </div><!-- #content -->
    </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
