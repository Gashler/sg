<!doctype html>
<html>
	<head>
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	</head>
	<body>
		<form action="/wp-content/themes/twentythirteen/break.php" method="post">
			<select name="type">
				<option value="2_line">2 line address</option>
				<option value="3_line">3 line address</option>
				<option value="census.gov">census.gov</option>
				<option value="greatschools">greatschools.org</option>
				<option value="mapquest">mapquest.com</option>
			</select>
			<textarea id="data" name="data"></textarea>
			<button id="submit">Rearrange</button>
		</form>
		<table id="table" border="1">
			<!--<thead>
				<th>Name of Company</th>
				<th>Address Line 1</th>
				<th>Address Line 2</th>
				<th>City</th>
				<th>State</th>
				<th>Zipcode</th>
			</thead>-->
			<tbody>
				<?php
					$type = $_POST['type'];
					$data = $_POST['data'];
					$data = explode('\"', $data);
					foreach($data as $entry) {
						$lines = explode("\n", $entry);
						$x = 1;
						$rows = 0;
						if ($type == "census.gov") {
							echo "census.gov";
							foreach($lines as $line) {
								//$line = strtolower($line);
								//$line = ucwords($line);
								if ($x == 1) {
									$line = str_replace("10. 	","",$line);
									$line = str_replace("11. 	","",$line);
									$line = str_replace("12. 	","",$line);
									$line = str_replace("13. 	","",$line);
									$line = str_replace("1. 	","",$line);
									$line = str_replace("2. 	","",$line);
									$line = str_replace("3. 	","",$line);
									$line = str_replace("4. 	","",$line);
									$line = str_replace("5. 	","",$line);
									$line = str_replace("6. 	","",$line);
									$line = str_replace("7. 	","",$line);
									$line = str_replace("8. 	","",$line);
									$line = str_replace("9. 	","",$line);
									$line = str_replace("Library System","",$line);
									$line = str_replace("Branch Library","",$line);
									$line = str_replace("Central Library","",$line);
									echo "<tr><td>" . $line . "</td>";
								}
								if ($x == 2) {
									$address = explode(", ", $line);
									echo "<td>" . $address[0] . "</td><td></td>";
									$city = explode(", ", $address[1]);
									echo "<td>" . $city[0] . "</td>";
									$state = explode(", ", $address[2]);
									$state = explode(" ", $state[0]);
									echo "<td>" . $state[0] . "</td>";
									$zip = explode(" ", $state[1]);
									echo "<td>" . $zip[0] . "</td><td></td><td>library</td>";
									echo "</tr>";
									$rows ++;
								}
								$x ++;
								if ($x == 3) $x = 1;
							}
						}
						if ($type == "greatschools") {
							echo "greatschools.org";
							foreach($lines as $line) {
								if ($x == 1) echo "<tr><td>" . $line . "</td>";
								if ($x == 3) echo "<td>" . $line . "</td><td></td>";
								if ($x == 4) {
									$cityStateZip = explode(", ", $line);
									echo "<td>" . $cityStateZip[0] . "</td>";
									$bob = $cityStateZip[1];
									$stateZip = explode(" ", $bob);
									echo "<td>" . $stateZip[0] . "</td><td>" . $stateZip[1] . "</td>";
									echo "</tr>";
								}
								$x ++;
								if ($line == "Add to My School List") $x --;
								if (strpos($line, "Rate this school") !== false) $x --;
								if (strpos($line, "Read more") !== false) $x -= 2;
								if (strpos($line,"Photo of") !== false) $x --;
								if (strpos($line," miles") !== false) $x --;
								if ($x == 8) $x = 1;
							}
						}
						if ($type == "2_line") {
							echo $type;
							foreach($lines as $line) {
								if ($x == 1) echo "<tr><td>" . $line . "</td>";
								if ($x == 2) {
									$addressCityStateZip = explode(", ", $line);
									echo "<td>" . $addressCityStateZip[0] . "</td><td></td>";
									$cityStateZip = explode(", ", $addressCityStateZip[1]);
									echo "<td>" . $cityStateZip[0] . "</td>";
									$stateZip = explode(", ", $addressCityStateZip[2]);
									$stateZip2 = explode(" ", $stateZip[0]);
									echo "<td>" . $stateZip2[0] . "</td><td>" . $stateZip2[1] . "</td>";
									echo "</tr>";
								}
								$x ++;
								if ($x == 3) $x = 1;
							}
						}
						if ($type == "3_line") {
							echo "3_line";
							foreach($lines as $line) {
								if ($x == 1) echo "<tr><td>" . $line . "</td>";
								if ($x == 2) echo "<td>" . $line . "</td><td></td>";
								if ($x == 3) {
									$cityStateZip = explode(", ", $line);
									echo "<td>" . $cityStateZip[0] . "</td>";
									$bob = $cityStateZip[1];
									$stateZip = explode(" ", $bob);
									echo "<td>" . $stateZip[0] . "</td><td>" . $stateZip[1] . "</td>";
									echo "</tr>";
								}
								$x ++;
								if ($x == 4) $x = 1;
							}
						}
						if ($type == "mapquest") {
							foreach($lines as $line) {
								if ($x == 1) echo "<tr><td>" . $line . "</td>";
								if ($x == 2) {
									$addressCityStateZip = explode(", ", $line);
									echo "<td>" . $addressCityStateZip[0] . "</td><td></td>";
									$cityStateZip = explode(", ", $addressCityStateZip[1]);
									echo "<td>" . $cityStateZip[0] . "</td>";
									$stateZip = explode(", ", $addressCityStateZip[2]);
									$stateZip2 = explode(" ", $stateZip[0]);
									echo "<td>" . $stateZip2[0] . "</td><td>" . $stateZip2[1] . "</td><td></td><td>library</td>";
									echo "</tr>";
									$rows ++;
								}
								if (strpos($line,"(") !== false || strpos($line,"More Info") !== false) $x --;
								if (strpos($line,"Call Today") !== false || strpos($line,"MapDirectionsSearch NearbySave") !== false) $x --;
								$x ++;
								if ($x == 3) $x = 1;
							}
						}
					}
				?>
			</tbody>
		</table>
		<?php
			echo "Number of Rows: " . $rows;
		?>
		<script>
			var type = "<?php echo $type ?>";
			if (type == "mapquest") {
				$("table tr").each(function() {
					if ($(" td", this).length == 1) $(this).remove();//$(this).parent().remove();
				});
			}
		</script>
	</body>
</html>