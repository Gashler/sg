<!doctype html>
<html>
	<head>
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	</head>
	<body>
		<form action="/wp-content/themes/twentythirteen/break.php" method="post">
			<select name="type">
				<option value="greatschools">greatschools.org</option>
				<option value="3_line">standard 3 line address</option>
			</select>
			<textarea id="data" name="data"></textarea>
			<button id="submit">Rearrange</button>
		</form>
		<table id="table" border="1">
			<!--<thead>
				<th>Name of Company</th>
				<th>Address Line 1</th>
				<th>Address Line 2</th>
				<th>City</th>
				<th>State</th>
				<th>Zipcode</th>
			</thead>-->
			<tbody>
				<?php
					$type = $_POST['type'];
					$data = $_POST['data'];
					$data = explode('\"', $data);
					foreach($data as $entry) {
						$lines = explode("\n", $entry);
						$x = 1;
						if ($type == "greatschools") {
							foreach($lines as $line) {
								if ($x == 1) echo "<tr><td>" . $line . "</td>";
								if ($x == 3) echo "<td>" . $line . "</td><td></td>";
								if ($x == 4) {
									$cityStateZip = explode(", ", $line);
									echo "<td>" . $cityStateZip[0] . "</td>";
									$bob = $cityStateZip[1];
									$stateZip = explode(" ", $bob);
									echo "<td>" . $stateZip[0] . "</td><td>" . $stateZip[1] . "</td>";
									echo "</tr>";
								}
								$x ++;
								if ($line == "Add to My School List") $x --;
								if (strpos($line,"Rate this school") !== false) $x --;
								if (strpos($line, "Read more") !== false) $x -= 2;
								if (strpos($line,"Photo of") !== false) $x --;
								if (strpos($line," miles") !== false) $x --;
								if ($x == 8) $x = 1;
							}
						}
						if ($type == "3_line") {
							foreach($lines as $line) {
								if ($x == 1) echo "<tr><td>" . $line . "</td>";
								if ($x == 2) echo "<td>" . $line . "</td><td></td>";
								if ($x == 3) {
									$cityStateZip = explode(", ", $line);
									echo "<td>" . $cityStateZip[0] . "</td>";
									$bob = $cityStateZip[1];
									$stateZip = explode(" ", $bob);
									echo "<td>" . $stateZip[0] . "</td><td>" . $stateZip[1] . "</td>";
									echo "</tr>";
								}
								$x ++;
								if ($x == 4) $x = 1;
							}
						}
					}
				?>
			</tbody>
		</table>
	</body>
</html>