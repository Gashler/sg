// convert links for landing pages to link to other pages in landing page format
// if (typeof landing !== 'undefined') {
//     if (landing == "1") {
//     	jQuery("a").each(function() {
//     		var href = jQuery(this).attr("href");
//     		var href = href + "?landing=1";
//     		jQuery(this).attr("href", href);
//     	});
//     }
// }

// pop
var popOpen = false;
function pop(content) {
	popOpen = true;
	jQuery("body").prepend("<div class='black'></div><div class='pop'><div class='x'>&times;</div>" + content + "</div>");
	jQuery(".black, .pop").fadeIn(function() {
		jQuery(".black, .x").click(function() {
			jQuery(".black, .pop").fadeOut(function() {
				closePopup();
			});
		});
	});
}
function closePopup() {
	popOpen = false;
	jQuery(".black, .pop").fadeOut(function() {
		jQuery(".black").remove();
		jQuery(".pop").hide();
	});
}
jQuery(".closePopup").click(function() {
	closePopup();
});
