<!doctype html>
<html>
	<head>
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	</head>
	<body>
		<form action="/wp-content/themes/twentythirteen/break.php" method="post">
			<textarea id="data" name="data"></textarea>
			<button id="submit">Rearrange</button>
		</form>
		<table id="table">
			<thead>
				<th>Name of Company</th>
				<th>Address Line 1</th>
				<th>City</th>
				<th>State</th>
				<th>Zipcode</th>
			</thead>
			<tbody>
				<?php
					$data = $_POST['data'];
					$data = explode($data, "\n");
					$x = 1;
					foreach($data as $line) {
						if ($x <= 5) echo "<tr>";
						echo "<td>" . $line . "</td>";
						$x ++;
						if ($x == 5) echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</body>
</html>